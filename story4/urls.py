from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('minigame/',views.minigame, name='minigame'),
    path('time/<str:timezone>/',views.time),
    path('time/',views.time)
]